FROM node:10.14

WORKDIR /app/

COPY package.json package-lock.json /app/
RUN npm i

COPY . /app/
VOLUME /app/storage

EXPOSE 8000

ENV NODE_ENV=production
ENV GOOGLE_APPLICATION_CREDENTIALS /app/storage/where-the-fuck-is-xur-ed4bd-firebase-adminsdk-bpfqa-48a603d98c.json

CMD npm start